import chevron
import click
import subprocess
import os
import re
import time
import kubeflow
import pathlib



@click.command()
@click.option(
    "--startfile",
    required=True,
    help="could be py or ipynb file"
)
@click.option(
    "--image",
    required=False,
    default="python:3.7"
)
@click.option(
    "--gpu",
    required=False,
    type=bool,
    default=False
)
@click.option(
    "--cores",
    required=False,
    type=int,
    default=1,
    help="request cores (1-20)"
)
@click.option(
    "--memory",
    required=False,
    type=int,
    default=1,
    help="request memory (GB)"
)
@click.option(
    "--just_create_yaml",
    required=False,
    type=bool,
    default=False,
    help="Just create yaml without uploading it to api and creating run"
)

def main(
        startfile: str,
        image: str,
        gpu: bool,
        cores: int,
        memory: int,
        just_create_yaml: bool
):
    interpreter = "python3"
    template_dir = os.path.join(pathlib.Path(__file__).parent.resolve(), "templates")
    prefix = os.environ.get("NB_PREFIX", "digits").split("/")
    namespace = prefix[2]
    filename = os.path.basename(startfile)
    workdir = os.path.dirname(os.path.abspath(os.path.expanduser(os.path.expandvars(startfile))))
    requirements = ""
    if os.path.isfile(os.path.join(workdir, 'requirements.txt')):
        requirements = "&& python3 -m pip install --upgrade pip && python3 -m pip install -r requirements.txt"
    mainfile = filename
    if os.path.splitext(filename)[1] == '.ipynb':
        subprocess.call(["ipython", "nbconvert", "--to", "script", "{}/{}".format(workdir, filename)])
        mainfile = "{}{}".format(os.path.splitext(filename)[0], '.py')
        interpreter = "ipython"
    with open(os.path.join(template_dir, "pipeline.yaml"), "r") as f:
        pipeline_template = f.read()
    pipeline_name = re.sub("[^0-9a-zA-Z]+", "-", mainfile)
    pipeline = chevron.render(
        template=pipeline_template,
        data=dict(
            pipeline_name=pipeline_name,
            workdir=workdir,
            mainfile=mainfile,
            namespace=namespace,
            requirements=requirements,
            image=image,
            gpu=gpu,
            cores=cores,
            memory=memory,
            timestamp=time.time(),
            interpreter=interpreter
        )
    )
    pipeline_file_path = '{}/{}.yaml'.format(workdir,pipeline_name)
    with open(pipeline_file_path, "w") as pipeline_file:
        pipeline_file.write(pipeline)
    print("Your pipeline saved to {}".format(pipeline_file_path))
    if not just_create_yaml:
        kubeflow.main(pipeline_file_path)

if __name__ == "__main__":
    main()
