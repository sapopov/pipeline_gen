import kfp
import os
import tarfile
import time

def main(pipeline_file: str):
    if not os.path.isfile(pipeline_file):
        raise Exception("No such file")
    client = kfp.Client()
    prefix = os.environ.get("NB_PREFIX", "digits").split("/")
    namespace = prefix[2]
    # geting/creating experiment
    try:
        experiment=client.get_experiment(experiment_name=namespace)
    except ValueError:
        print("no such experiment, creating..")
        client.create_experiment(
            name=namespace,
            description='Auto-created experiment by pipelines-script',
            namespace=namespace
        )
        experiment=client.get_experiment(experiment_name=namespace)



    # geting/creating pipeline
    # let`s pack our pipeline
    pipeline_tar = '/tmp/pipeline-{}.tar.gz'.format(time.time())
    with tarfile.open(pipeline_tar, "w:gz") as tar:
            tar.add(pipeline_file, arcname='pipeline.yaml')
    try:
        pipeline_id=client.get_pipeline_id(name=namespace)
        pipeline=client.get_pipeline(pipeline_id=pipeline_id)
        # upload pipeline version
        print("uploading pipeline_version")
        pipeline=client.upload_pipeline_version(
            pipeline_package_path=pipeline_tar,
            pipeline_version_name="{}-{}".format(namespace,time.time()),
            pipeline_id=pipeline_id
        )
    except ValueError:
        print("no such pipeline, creating..")
        # upload new pipeline
        # archive our pipeline

        pipeline=client.upload_pipeline(
            pipeline_package_path=pipeline_tar,
            pipeline_name=namespace,
            description='Auto-created pipeline by pipelines-script'
        )
    #print(pipeline)
    os.remove(pipeline_tar)
    run=client.run_pipeline(
        experiment_id=experiment.id,
        job_name="run-{}".format(time.time()),
        version_id=pipeline.id,
    )

    print("Done, check your run here -  https://kubeflow.coresearch.club/_/pipeline/?ns={}#/runs/details/{}".format(namespace,run.id))


